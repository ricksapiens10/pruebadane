Buen día, 


Por medio del presente envío prueba de conocimiento.


Detalles de la prueba 

El desarrollador debe implementar la prueba técnica que se adjunta a este correo.

Debe generar un repositorio en Gitlab con la prueba en donde debe reposar además del desarrollo la documentación del mismo y enviar la prueba al correo rjguzmanm@dane.gov.co.

Esta prueba está diseñada para ser entregada en 8 horas, desde el momento de confirmación de recepción de la prueba.

Estaremos atentos a la solución de la prueba para poder continuar el proceso de aplicación.


Atentamente,


-- Rafael.
